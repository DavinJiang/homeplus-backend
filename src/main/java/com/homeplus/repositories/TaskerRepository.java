package com.homeplus.repositories;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskerEntity;
import com.homeplus.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TaskerRepository extends JpaRepository<TaskerEntity, Long> {

    @Query("select t from TaskerEntity as t where t.id = :id")
    Optional<TaskerEntity> findById(@Param("id") Long id);

    @Query("select t from TaskerEntity as t where t.userEntity = :userEntity")
    Optional<TaskerEntity> findByUser(UserEntity userEntity);
}
