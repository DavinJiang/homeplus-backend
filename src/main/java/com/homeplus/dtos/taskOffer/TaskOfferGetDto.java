package com.homeplus.dtos.taskOffer;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskerEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@NoArgsConstructor
public class TaskOfferGetDto {
    private Long id;

    private TaskerEntity taskerEntity;

    private TaskEntity taskEntity;

    private String offered_price;

    private String offer_status;

    private String description;

    private String reply_message;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;
}
