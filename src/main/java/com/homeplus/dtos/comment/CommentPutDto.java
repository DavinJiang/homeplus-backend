package com.homeplus.dtos.comment;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommentPutDto {
    private Long id;

    private UserEntity userEntity;

    private TaskEntity taskEntity;

    private String message;

    private OffsetDateTime created_time;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
