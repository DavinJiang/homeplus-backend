package com.homeplus.models;

public enum TaskStatus {
    open,
    canceled,
    assigned,
    completed,
}
