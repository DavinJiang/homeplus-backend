DROP TABLE IF EXISTS "task_review";
CREATE TABLE "task_review" (
    "review_id" SERIAL PRIMARY KEY,
    "task_id" INT REFERENCES "task_post" ("task_id"),
    "review" VARCHAR(255),
    "rating" INT,
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL
);